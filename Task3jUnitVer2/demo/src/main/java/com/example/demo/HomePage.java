package com.example.demo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage extends BasePage {

    // Web elements are initialized as class members
    @FindBy(css = "div.ND91id")
    private WebElement searchIcon;

    @FindBy(name = "q")
    private WebElement searchField;

    public HomePage(WebDriver driver) {
        super(driver);
        // Initialize web elements using PageFactory
        PageFactory.initElements(driver, this);
    }

    public void open() {
        driver.get("https://cloud.google.com/");
    }

    public SearchResultsPage searchForPricingCalculator(String query) {
        searchIcon.click();
        searchField.sendKeys(query);
        searchField.submit();
        return new SearchResultsPage(driver);
    }
}
