package com.example.demo;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.Set;

public class PricingCalculatorPage extends BasePage {

    @FindBy(xpath = "//span[text()='Add to estimate']")
    private WebElement addToEstimateButton;

    @FindBy(xpath = "//h2[text()='Compute Engine']")
    private WebElement computeEngineButton;

    @FindBy(css = ".gt0C8e.MyvX5d.D0aEmf")
    private WebElement totalCostElement;

    @FindBy(xpath = "//input[@id='c11']")
    private WebElement numberOfInstancesInput;
    @FindBy(xpath = "//div[@data-field-type='106']")
    private WebElement operatingSystemDropdown;

    @FindBy(css = "button.glue-cookie-notification-bar__accept")
    private WebElement acceptCookiesButton;

    @FindBy(xpath = "//*[@id=\"ucj-1\"]/div/div/div/div/div/div/div/div[1]/div/div[2]/div[3]/div[9]/div/div/div[2]/div/div/div[1]/label")
    private WebElement provisioningModelInput;

    @FindBy(css = "[jsname='kgDJk']")
    private WebElement machineTypeDropdown;


//Trying to find "Add GPUs" button without xpath, none of the options works
    //@FindBy(css = "span[jsname='m9ZlFb']")
    //@FindBy(xpath = "//span[@jsname='m9ZlFb']")
    //@FindBy(className = "RBHQF-ksKsZd")
    //@FindBy(xpath = "//span[@data-unbounded='false' and @jsname='m9ZlFb']")
    //@FindBy(xpath = "//div[@jsname='U7okFc']//span[@jsname='m9ZlFb']")
    @FindBy(xpath = "//*[@id=\"ow4\"]/div/div/div/div/div/div/div[1]/div/div[2]/div[3]/div[21]/div/div/div[1]/div/div/span/div/button/div")
    private WebElement addGPUsCheckbox;

//Trying to find GPU Model dropdown, none of the options works
    //@FindBy(id = "i35")
    //@FindBy(xpath = "//input[@id='i35']")
    //@FindBy(className = "O1htCb-H9tDt PPUDSe t8xIwc")
    //@FindBy(className = "YgByBe")
    //@FindBy(css = "parentUniqueClassOrId > div.VfPpkd-aPP78e")
    //@FindBy(xpath = "//div[@class='VfPpkd-TkwUic']/following-sibling::div[@class='VfPpkd-aPP78e']")
    //@FindBy(css = "div.VfPpkd-TkwUic + div.VfPpkd-aPP78e")
    //@FindBy(css = "[aria-label='GPU Model']")
    //@FindBy(xpath = "//span[@jsname='B9mpmd']")
    //@FindBy(css = "[jsname='B9mpmd']")
    //@FindBy(css = "[jsname='kgDJk']")
    @FindBy(xpath = "//*[@id=\"ow4\"]/div/div/div/div/div/div/div[1]/div/div[2]/div[3]/div[23]/div/div[1]/div/div/div")
    private WebElement gpuModelDropdown;

    @FindBy(xpath = "//*[@id=\"ow4\"]/div/div/div/div/div/div/div[1]/div/div[2]/div[3]/div[24]/div")
    private WebElement gpuNumberDropdown;

    @FindBy(css = "[placeholder-id='ucc-72']")
    private WebElement ssdTypeDropdown;

    @FindBy(css = "[placeholder-id='ucc-78']")
    private WebElement datacenterLocationDropdown;

    @FindBy(css = "label[for='1-year']")
    private WebElement committedUseDropdown;

    @FindBy(css = "button[aria-label='Open Share Estimate dialog']")
    private WebElement shareButton;

    @FindBy(xpath = "//a[contains(text(), 'Open estimate summary')]")
    private WebElement openEstimateSummaryButton;

    public PricingCalculatorPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void clickAddToEstimate() {
        addToEstimateButton.click();
    }

    public void clickComputeEngine() {
        computeEngineButton.click();
    }

    public void acceptCookies() {
        try {
            acceptCookiesButton.click();
        } catch (Exception e) {
            System.out.println("Cookie notification bar not found or already accepted.");
        }
    }

    public void fillNumberOfInstances(int number) {

        numberOfInstancesInput.clear();
        numberOfInstancesInput.sendKeys(String.valueOf(number));

    }

    public void selectOperatingSystem(String operatingSystem) {
        operatingSystemDropdown.click();
        WebElement option = driver.findElement(By.xpath("//span[contains(text(), '" + operatingSystem + "')]"));
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", option);

    }

    public void selectMachineType(String machineType) {
        machineTypeDropdown.click();
        WebElement machineTypeOption = driver.findElement(By.xpath("//li[@data-value='" + machineType + "']"));
        machineTypeOption.click();
    }

    public void clickCheckbox() {
        addGPUsCheckbox.click();
    }

    public void selectGpuModel(String gpuModel) {
        gpuModelDropdown.click();
        WebElement option = driver.findElement(By.xpath("//li[@data-value='" + gpuModel + "']"));
        option.click();
    }

    public void selectNumberOfGPU(String gpuNumber) {
        gpuNumberDropdown.click();
        WebElement option = driver.findElement(By.xpath("//li[@data-value='" + gpuNumber + "']"));
        option.click();
    }

    public void selectSSD(String ssdType) {
        ssdTypeDropdown.click();
        WebElement option = driver.findElement(By.xpath("//li[contains(.,'" + ssdType + "')]"));
        option.click();
    }

    public void selectDatacenterLocation(String location) {
        datacenterLocationDropdown.click();
        WebElement option = driver.findElement(By.xpath("//li[contains(.,'" + location + "')]"));
        option.click();
    }

    public void committedUse() {
        committedUseDropdown.click();
    }

    public double retrieveTotalEstimatedCost() {
        String totalCostText = totalCostElement.getText();
        String costValue = totalCostText.replaceAll("[^0-9.]", "");
        return Double.parseDouble(costValue);
    }

    public void clickShareButton() {
        shareButton.click();
    }

    public void clickOpenEstimateSummaryButton() {
        openEstimateSummaryButton.click();
        Set<String> handles = driver.getWindowHandles();
        for (String handle : handles) {
            if (!handle.equals(driver.getWindowHandle())) {
                driver.switchTo().window(handle);
                break;
            }
        }
    }

    public double extractNewEstimatedCost() {
        WebElement newCostElement = driver.findElement(By.xpath("//*[@id=\"yDmH0d\"]/c-wiz[1]/div/div/div/div/div[1]/div/div[1]/div[1]/h4"));
        String newCostText = newCostElement.getText().replace("[^0-9.]", "").replace(",", "");
        return Double.parseDouble(newCostText);
    }
}
