package com.example.demo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SearchResultsPage extends BasePage {
    private By pricingCalculatorLink = By.linkText("Google Cloud Pricing Calculator");

    public SearchResultsPage(WebDriver driver) {
        super(driver);
    }

    public PricingCalculatorPage goToPricingCalculatorPage() {
        WebElement pricingCalculatorLinkElement = driver.findElement(pricingCalculatorLink);
        pricingCalculatorLinkElement.click();

        return new PricingCalculatorPage(driver);
    }
}
